<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ranking extends Model
{
    protected $fillable = [ 
        'id','user_id', 'pontos', 'tempo' 
    ];

    public function user() 
    {
        return $this->belongsTo('App\User');   
        
    }    
    
    
}
