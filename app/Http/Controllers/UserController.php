<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse; 
use Illuminate\Support\Facades\Validator;  
use Illuminate\Support\Facades\Cookie; 
use Carbon\Carbon;  
use App\User;
Use Auth;
use Socialite; 

class UserController extends Controller 
{
    public function cadastro(Request $request){
        $user = $request->all();
        $user['password'] = bcrypt($user['password']);
        
        $user = User::create($user);

        $accessToken = $user->createToken('authToken')->accessToken;


        return response()->json(['user'=>$user,'accessToken'=>$accessToken]); 
    }

    public function update(Request $request, $id)
    {

        $user  = User::find($id); 
        $user->fill($request->all()); 
        $user->save(); 

        return "ok";  
    }

    public function index(Request $request){
        return User::all(); 
    }

    public function me(Request $request){
        return response()->json(['user'=>auth()->user()]);
    }

    public function login(Request $request){

        Validator::make($request->all(),[ 
            "email"=>"email|required",
            "password"=>"required"
        ])->validate();    

  
        if (!auth()->attempt($request->all())){
            return response(["message"=>"Dados inválidos"]);
        }

        $user = auth()->user();
        $accessToken = $user->createToken('authToken')->accessToken; 

        return response()->json(['user'=>$user,'accessToken'=>$accessToken]); 

    }


    public function providers(){

        return response()->json(['providers'=>['google','facebook'],'urlLogin'=>env('APP_URL').'/redirect']);   
    }

    public function redirectToProvider($provider) 
    { 
        return Socialite::driver($provider)->redirect(); 
    }



    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request, $provider)
    {

        //try {
            $user = Socialite::driver($provider)->user();
        //} catch (\Exception $e) {
          //  return redirect('/login');
        //  return "<html><head><script>localStorage.token = ''; window.location='".env('APP_URL')."' </script></head></html>";
       // }  

        //return response()->json(["user"=>$user]);
        


        // only allow people with @company.com to login
        if(explode("@", $user->email)[1] !== 'company.com'){
         //   return redirect()->to('/');
        }
 
        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();
        if(!$existingUser){
            $newUser                  = new User;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->avatar           = $user->avatar; 
            $newUser->google_id       = $user->id; 
            $newUser->save();
            $existingUser = $newUser;
        }else{ 
            $existingUser->avatar     = $user->avatar; 
            $existingUser->google_id  = $user->id; 
            $existingUser->save();
        } 

          
        $accessToken = $existingUser->createToken('authToken')->accessToken; 

   

        // return response()->json(['user'=>$existingUser,'accessToken'=>$accessToken]); 

        //return "<html><head><script>localStorage.token = '".$accessToken."'; window.location='".env('APP_URL')."' </script></head></html>";

        $request->session()->put('state', $accessToken); 

        //var_dump(urldecode($accessToken));    
        //die;  

        //'name', 'value', $minutes, $path, $domain, $secure, $httpOnly
        Cookie::queue(Cookie::make('tokenCookie', $accessToken , 5, null, null, null, false));  

        return redirect()->away(env('APP_URL')."/#/login-success-provider");          
        
        

        
    }

    public function getTempToken(Request $request)
    {
        return response()->json(["token"=>$request->session()->pull('state')]);     
    }    
}
