<?php

namespace App\Http\Controllers;

use App\Pergunta;
use App\Materia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;    

class PerguntaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         

         return Materia::with(["perguntas" => function ($query) use($request) {

            if ($request->get("verificadas") == "true"){
                $query->where('verificada', 1); 
            }

            $query->orderBy('id', 'ASC');
            //$query->inRandomOrder();


        } ])->orderBy('id', 'ASC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $pergunta = $request->all();
        
        $pergunta = Pergunta::create($pergunta); 

        return response()->json(['pergunta'=>$pergunta]);         

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pergunta  $pergunta
     * @return \Illuminate\Http\Response
     */
    public function show(Pergunta $pergunta)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pergunta  $pergunta
     * @return \Illuminate\Http\Response
     */
    public function edit(Pergunta $pergunta)
    {

        return $pergunta; 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pergunta  $pergunta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pergunta $pergunta)
    {

        Validator::make($request->all(),[ 
            "materia_id"=>"required",
            "pergunta"=>"required",
            "ra"=>"required",
            "rb"=>"required",
            "rc"=>"required",
            "rd"=>"required",
            "re"=>"required", 
            "rcorreta"=>"required"
        ])->validate();     

        $pergunta->fill($request->all());    
        $pergunta->save(); 
        return $pergunta;   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pergunta  $pergunta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pergunta $pergunta)
    {
        //
    }
}
