/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');



window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
 
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Router from 'vue-router' 
import VueMask from 'v-mask'
import router from './router' 
import store from './store' 
import App from './app.vue'
import VueCookies from 'vue-cookies'


Vue.use(Router)  
Vue.use(VueCookies)  
Vue.use(VueMask)


axios.defaults.timeout = 30 * 1000; //Milisegundos 

// Add a request interceptor
axios.interceptors.request.use(function (config) {
    config.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');  
    return config; 
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

// Add a response interceptor 
axios.interceptors.response.use(function (response) { 
    return response;
  }, function (error) { 
    return Promise.reject(error); 
    });


    router.beforeEach((to, from, next) => {
	
    
        // Se a rota exige login
        if (to.matched.some(record => record.meta.requiresAuth)) {
    
            axios.get('/api/me').then(function(response){
                
                let usuario = response.data.user;  

                store.commit('setUsuarioLogado',response.data.user);  
    
                // Redireciona o usuário para atualização cadastral
                /*if ( (!usuario.telefone || !usuario.name) && to.name != 'atualiza_cadastro'){  
                    M.toast({html: '<span>Necessário atualizar seu perfil!</span>'}); 
                    next({name: 'atualiza_cadastro'})
                }*/
                
                // Se o usuário possui o perfil entre os exigidos pela rota
                let perfilUsuarioLogado = "USER"
                if (to.meta.requiresAuth.split(",").indexOf(perfilUsuarioLogado) != -1){  
                    next()
                }else{
                    M.toast({html: '<span>Você não possui o perfil necessário para acessar este conteúdo!</span>'}); 
                    next({name: 'home'})
                }
                        
            }, function(error){
                //M.toast({html: '<span>Usuário não autenticado. Login necessário!</span>'});
                window.localStorage.redirectAfterLogin = to.path;   
                window.location = "/#/login"; 
            });
    
        } else { // Se não exige login 
              next();
        }
    });      
    
const app = new Vue({
    router,
    store,
    el: '#app', 
    render: h=>h(App),
    
    methods: {
        insertHelperText: function (element,message,color){

            var text_color = "";
            if (!color){
              text_color = "red-text";
            }else{
              text_color = color+"-text";
            } 
            if ($(element).next().hasClass("helper-text")){
               $(element).next().remove(); 
               }
             $(element).after('<span class="helper-text '+text_color+'" style="display:none">'+message+'</span>');  
             $(element).next().fadeIn(500);
           },
           removeHelperText: function(element){ 
            if ($(element).next().hasClass("helper-text")){
               $(element).next().remove(); 
               }
           }        
    }
});

