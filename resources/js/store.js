import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

    spinner: {show:false},
    usuarioLogado:'', 
    token : localStorage.getItem('token') || '', 

  },
  mutations: { 
		setSpinner : function(state, spinner){ 
			console.log("state.spinner=", spinner)
			state.spinner = spinner;  
    },
		setUsuarioLogado : function(state, usuarioLogado){
			console.log("state.usuarioLogado=", usuarioLogado)
			state.usuarioLogado = usuarioLogado; 
    },
		setToken: function(state, token){
			state.accessToken = token;   
		},    

		setLogout : function(state){
      state.usuarioLogado = '';    
      state.token = "";
      localStorage.removeItem('token'); 
      console.log('store.setLogout concluido'); 

		}          

  },
  actions: {

  }
})
