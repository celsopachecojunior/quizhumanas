import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue' 
import Sugestao from './views/Sugestao.vue' 
import AtualizaCadastro from './views/AtualizaCadastro.vue'
import PerguntaList from './views/PerguntaList.vue'
import PerguntaCreate from './views/PerguntaCreate.vue'
import PerguntaEdit from './views/PerguntaEdit.vue'
import Game from './views/Game.vue'
import GameWin from './views/GameWin.vue'
import GameOver from './views/GameOver.vue'
import LoginSuccessProvider from './views/LoginSuccessProvider.vue'  


 
export default new Router({ 
  routes: [
    { path: '/',      name: 'home',      component: Home },
    { path: '/login',      name: 'login',       component: Login },    
    { path: '/login-success-provider',      name: 'login-success-provider',       component: LoginSuccessProvider },
    { path: '/sugestao',      name: 'sugestao',       component: Sugestao },
    { path:"/atualiza-cadastro",  name:"atualiza-cadastro",   component:AtualizaCadastro, meta: { requiresAuth: 'USER,ADM' } },
    { path:"/pergunta",  name:"pergunta",   component:PerguntaList, meta: { requiresAuth: 'USER,ADM' } },
    { path:"/pergunta-create",  name:"pergunta-create",   component:PerguntaCreate, meta: { requiresAuth: 'USER,ADM' } },
    { path:"/pergunta-edit/:id",  name:"pergunta-edit",   component:PerguntaEdit, meta: { requiresAuth: 'USER,ADM' } },
    { path:"/game",  name:"game",   component:Game, meta: { requiresAuth: 'USER,ADM' } },
    { path:"/game-win/:pontos/:tempo",  name:"game-win",   component:GameWin, meta: { requiresAuth: 'USER,ADM' } },
    { path:"/game-over/:pontos/:tempo",  name:"game-over",   component:GameOver, meta: { requiresAuth: 'USER,ADM' } }
  ], 
  //mode: 'history', 
  scrollBehavior (to, from, savedPosition) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve( savedPosition || { x: 0, y: 0 } )
      }, 1000)
    })
  }
  
})
