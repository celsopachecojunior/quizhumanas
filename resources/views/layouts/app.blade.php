<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"> 

	<!-- favicon -->
	<link href="/images/favicon.ico" rel="icon" type="image/x-icon" /> 

    <title>{{ config('app.name', '') }}</title>   

	
	<!-- Scripts -->
	<script>

	</script>
     
    <script src="{{ asset('js/app.js') }}" defer></script> 

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
 
	<header></header>

	<main>
		 @yield('content') 
	</main>	 

	<footer class="page-footer" style="padding-top:5px;">
	  <div class="footer-copyright "  >
			<div class="container">

				<div class="center">
					<a class="white-text" href="/" style="cursor:default;">{{ env('APP_NAME', '') }}</a>
				</div>
			
				<div class="center" style="margin-top:10px;">
					<a class="white-text btn-flat" href="" onclick="return false" target="_blank"><i class="fab fa-whatsapp fa-2x p-2 left"></i>WhatsApp</a> 
					&nbsp;
					<a class="white-text btn-flat" href="" onclick="return false" target="_blank"><i class="fab fa-facebook fa-2x p-2 left"></i>Facebook</a>
					&nbsp;
					<a class="white-text btn-flat" href="" onclick="return false" target="_blank"><i class="fab fa-instagram fa-2x p-2 left"></i>Instagram</a> 
					&nbsp;
					<i class="white-text" class="fab fa-twitter-square fa-2x p-2"></i>

				</div> 

			</div>
	  </div>
	</footer>

	<!-- Modal -->
	<div id="main_modal" class="modal modal-fixed-footer">
		<div class="modal-content">
		<h4 id="main_modal_title"></h4>
		<div id="main_modal_content"></div>
		</div>
		<div class="modal-footer">
		<a href="#!" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
		</div>
	</div>
	<!-- /Modal --> 

</body>
		
</html>
