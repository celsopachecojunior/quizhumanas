<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cadastro', 'UserController@cadastro');
Route::get('/providers', 'UserController@providers');  
Route::post('/login', 'UserController@login'); 
 


Route::resource('rankings', 'RankingController');     
Route::resource('sugestaos', 'SugestaoController');  

Route::middleware(['auth:api'])->group( function () {
    Route::get('/user', 'UserController@index');
    Route::get('/me', 'UserController@me'); 
    
    // Usuario
    Route::match(['put', 'patch'], '/user/{id}','UserController@update');  
    Route::resource('perguntas', 'PerguntaController');    
});

Route::get('/perguntas', 'PerguntaController@index');  
//Route::get('/pergunta/{pergunta}/edit', 'PerguntaController@edit');   

