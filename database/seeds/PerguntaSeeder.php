<?php

use Illuminate\Database\Seeder;

class PerguntaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('perguntas')->insert([ 
            
            ['materia_id'=>	2	, 'pergunta' => 'Quem foi considerado o primeiro filósofo pré-socrático?', 'ra'=>'Tales de Mileto', 'rb'=>'Anaxímandro', 'rc'=>'Anaxímenes', 'rd'=>'Heráclito', 're'=>'Anaxágoras', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => 'Qual filosofo é o autor da frase "Só sei que nada sei" ?', 'ra'=>'Aristóteles', 'rb'=>'Platão', 'rc'=>'Sócrates', 'rd'=>'Karl Marx', 're'=>'Nietzche', 'rcorreta'=>'C', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => 'Qual filósofo é o autor da frase: "Penso, logo existo."', 'ra'=>'Sócrates', 'rb'=>'Platão', 'rc'=>'René Descartes', 'rd'=>'Santo Agostinho', 're'=>'Francis Bacon', 'rcorreta'=>'C', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => ' Qual filósofo é o autor da frase: "o homem nasce bom, e a sociedade o corrompe."? ', 'ra'=>'Karl Marx', 'rb'=>'Santo Agostinho', 'rc'=>'Jean Jacques Rousseau', 'rd'=>'Platão', 're'=>'Hannah Arendt', 'rcorreta'=>'C', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => 'O que a Antropologia filosófica procura responder?', 'ra'=>'Quem é Aristóteles?', 'rb'=>'Qual o sentido da natureza humana?', 'rc'=>'O que é o Dualismo Platônico?', 'rd'=>'O que é a metafísica?', 're'=>'O que é o amor?', 'rcorreta'=>'B', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => 'Que filósofo foi o autor da frase: "A graça de Deus não encontra homens aptos para a salvação, mas nos torna aptos para recebê-las"', 'ra'=>'Platão', 'rb'=>'Sócrates', 'rc'=>'Karl Marx', 'rd'=>'Rousseau', 're'=>'Santo Agostinho', 'rcorreta'=>'E', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => 'Qual filósofo é o autor da frase: "O homem é por natureza um animal político"?', 'ra'=>'Platão ', 'rb'=>'Rousseau', 'rc'=>'Francis Bacon', 'rd'=>'Aristóteles ', 're'=>'Sócrates ', 'rcorreta'=>'D', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => 'A qual filósofo pertence a frase: "Penso, logo existo"?', 'ra'=>'Sócrates ', 'rb'=>'Aristóteles ', 'rc'=>'René Descartes', 'rd'=>'Rousseau', 're'=>'Platão ', 'rcorreta'=>'C', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => 'Qual era a visao do homem para Karl Marx? ', 'ra'=>'O Bom Selvagem', 'rb'=>'Um animal político ', 'rc'=>'Dividido entre corpo e alma.', 'rd'=>'Um ser social', 're'=>'O que duvida de tudo e de todos.', 'rcorreta'=>'D', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => 'Quem é o autor da frase "Só sei que nada sei"?', 'ra'=>'Sócrates ', 'rb'=>'Aristóteles', 'rc'=>'Epicuro', 'rd'=>'Karl Marx', 're'=>'Voltaire', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => 'Qual país é considerado o berço da Filosofia Ocidental?', 'ra'=>'Grécia ', 'rb'=>'França ', 'rc'=>'Portugal', 'rd'=>'Alemanha', 're'=>'Polônia ', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => 'O que é Metafísica?', 'ra'=>'São as relações de produção ', 'rb'=>'Movimento de artistas criado em Atenas', 'rc'=>'Estuda o homem em sua dimensão física', 'rd'=>'É a busca pela origem do ser humano', 're'=>'Estuda o homem em sua dimensão cultural.', 'rcorreta'=>'D', 'verificada'=>1],
            ['materia_id'=>	2	, 'pergunta' => 'A frase "Sábio é aquele que conhece os limites da própria ignorância" é do filósofo:', 'ra'=>'Platão', 'rb'=>'Sócrates', 'rc'=>'Aristóteles', 'rd'=>'Karl Marx', 're'=>'Hannah Arendt', 'rcorreta'=>'B', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'O que marcou a passagem da Pré História para a Antiguidade:', 'ra'=>'Queda de Roma em poder dos bárbaros (476)', 'rb'=>'Aparecimento da escrita +/- 4000 a.c', 'rc'=>'Tomada de Constantinopla', 'rd'=>'Revolução Francesa', 're'=>'O nascimento de Cristo', 'rcorreta'=>'B', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'O que é Heliocentrismo?', 'ra'=>'Teoria que coloca o sol no centro do sistema solar', 'rb'=>'A centralização do poder real ', 'rc'=>'Teoria do Piloto anônimo ', 'rd'=>'A Teoria do direito divino dos Reis', 're'=>'A formação das monarquias nacionais ', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'O que a cronologia estuda?', 'ra'=>'Estuda as moedas', 'rb'=>'Estuda as famílias', 'rc'=>'Estuda o homem em sociedade', 'rd'=>'Estuda a localização dos fatos no tempo(datas)', 're'=>'Estuda documentos oficiais', 'rcorreta'=>'D', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'Quando foi o descobrimento da América?', 'ra'=>'1492', 'rb'=>'1325', 'rc'=>'1500', 'rd'=>'1645', 're'=>'1822', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'O que a história estuda?', 'ra'=>'Todos os elementos deixados pelo homem', 'rb'=>'Estuda o futuro', 'rc'=>'Os documentos oficiais', 'rd'=>'O passado da humanidade', 're'=>'Os mitos e lendas', 'rcorreta'=>'D', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'Quando foi a Guerra das Rosas?', 'ra'=>'1353 até 1385', 'rb'=>'1499 até 1551', 'rc'=>'1455 até 1485', 'rd'=>'1359 até 1360', 're'=>'1555 até 1565', 'rcorreta'=>'C', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'Os países pioneiros das grandes navegações foram: ', 'ra'=>'China e Índia', 'rb'=>'Uruguai e Venezuela ', 'rc'=>'Itália e Portugal ', 'rd'=>'Portugal e Espanha', 're'=>'Espanha e China ', 'rcorreta'=>'D', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'Qual foi o documento assinado após o fim da Primeira Guerra Mundial?', 'ra'=>'Tratado de Tordesilhas ', 'rb'=>'Tratado de Schengen', 'rc'=>'Tratado de Versalhes', 'rd'=>'Tratado de Methuen', 're'=>'Tratado de Munique', 'rcorreta'=>'C', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'Que invasão desencadeou a Segunda Guerra Mundial?', 'ra'=>'O ataque dos Estados Unidos ao Japão ', 'rb'=>'Invasão à França ', 'rc'=>'Invasão à Varsóvia (Polônia)', 'rd'=>'Invasão à Inglaterra', 're'=>'Invasão ao Império Austro-Húngaro', 'rcorreta'=>'C', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'Qual era o objetivo das Navegações Portuguesas antes de chegar ao Brasil?', 'ra'=>'Encontrar ouro', 'rb'=>'Encontrar objetos de luxo na América ', 'rc'=>'Procurar abrigo', 'rd'=>'Chegar até as Índias ', 're'=>'Encontrar o Pau-Brasil', 'rcorreta'=>'D', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'Quais países correspondiam às antigas "Índias"?', 'ra'=>'China, Japão e Índia', 'rb'=>'Iraque, Paquistão e Qatar', 'rc'=>'Índia, Egito e África ', 'rd'=>'Brasil,Colombia e México ', 're'=>'México, Panamá e Bolívia ', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'Qual a última batalha da Força Expedicionária Brasileira na Segunda Guerra mundial?', 'ra'=>'Batalha de Montese ', 'rb'=>'Batalha de Monte Castello', 'rc'=>'Batalha Monte Cassino', 'rd'=>'Batalha de Fornovo di Taro', 're'=>'Batalha de Trincheiras ', 'rcorreta'=>'D', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'O que a Numismática estuda? ', 'ra'=>'Estuda os documentos oficiais', 'rb'=>'Estuda as cédulas, moedas e medalhas ', 'rc'=>'Estuda o homem em sociedade', 'rd'=>'Estuda os brasões e escudos ', 're'=>'Estuda a localização dos fatos no tempo', 'rcorreta'=>'B', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'São exemplos de especiarias?', 'ra'=>'Cravo, canela, pimenta e gengibre', 'rb'=>'Ouro e prata ', 'rc'=>'Colônias para dominar e explorar ', 'rd'=>'Madeira e cobre', 're'=>'Jóias e pérolas', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'Em que ano Lutero publicou as 95 Teses ?', 'ra'=>'1545', 'rb'=>'1513', 'rc'=>'1517', 'rd'=>'1617', 're'=>'1524', 'rcorreta'=>'C', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => ' O escrivão oficial da frota portuguesa de Pedro Álvares Cabral era:', 'ra'=>'Luís de Camões ', 'rb'=>'Pero Vaz de Caminha', 'rc'=>'Vasco da Gama', 'rd'=>'Cristóvão Colombo', 're'=>'Américo Vespúcio', 'rcorreta'=>'B', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'A partir de que século começou a crise da Idade Média? ', 'ra'=>'Seculo XII', 'rb'=>'Seculo XVI', 'rc'=>'Seculo XI', 'rd'=>'Seculo XIV', 're'=>'Seculo XVII', 'rcorreta'=>'D', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'O navegador Vasco da Gama chegou às Índias em:', 'ra'=>'1469', 'rb'=>'1498', 'rc'=>'1468', 'rd'=>'1528', 're'=>'1479', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'Qual navegador português conseguiu realizar o périplo africano? ', 'ra'=>'Vasco da Gama', 'rb'=>'Américo Vespúcio ', 'rc'=>'Pedro Álvares Cabral ', 'rd'=>'Pero Vaz de Caminha', 're'=>'Duarte Pacheco ', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'Uma das principais críticas de Martinho Lutero à Igreja Católica foi a:', 'ra'=>'Venda de Indulgências', 'rb'=>'Venda de túmulos', 'rc'=>'Venda de terras', 'rd'=>'Catequização dos índios', 're'=>'Venda de especiarias', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'São períodos da Pré-História: ', 'ra'=>'Mesozóico e pré-cambriano', 'rb'=>'Paleolítico e Neolítico', 'rc'=>'Proterozóico e Cenozóico', 'rd'=>'Neolítico e Geolítico ', 're'=>'Paleolítico e Mesolítico', 'rcorreta'=>'B', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'A obra Os Lusíadas, inspirada na viagem de Vasco da Gama, foi escrita por:', 'ra'=>'José de Alencar ', 'rb'=>'Fernando Pessoa', 'rc'=>'Castro Alves', 'rd'=>'Dom Casmurro', 're'=>'Luís de Camões', 'rcorreta'=>'E', 'verificada'=>1],
            ['materia_id'=>	1	, 'pergunta' => 'A Guerra das Duas Rosas, na Inglaterra, foi entre as famílias:', 'ra'=>'Bourbon e Bragança', 'rb'=>'Capeto e Valois', 'rc'=>'York e Lancaster', 'rd'=>'Castela e Aragão ', 're'=>'Montecchio e Capuleto', 'rcorreta'=>'C', 'verificada'=>1],
            ['materia_id'=>	3	, 'pergunta' => 'A visão etnocêntrica pode levar:', 'ra'=>'À intolerância e à discriminação.', 'rb'=>'Ao reconhecimento de culturas diferentes.', 'rc'=>'À igualdade dos povos', 'rd'=>'Ao respeito às diversidades culturas', 're'=>'Ao relativismo cultural', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	3	, 'pergunta' => 'Pode ser considerada cultura material ou bem material:', 'ra'=>'Os sambas-enredo', 'rb'=>'O modo artesanal de fazer pão de queijo em Minas Gerais.', 'rc'=>'As fotografias do RJ no século XX', 'rd'=>'O frevo em Pernambuco', 're'=>'A Capoeira', 'rcorreta'=>'C', 'verificada'=>1],
            ['materia_id'=>	3	, 'pergunta' => 'A frase que corresponde a uma visão etnocêntrica é: ', 'ra'=>'Não gosto de sertanejo. ', 'rb'=>'Acredito em minha religião.', 'rc'=>'Gosto de funk', 'rd'=>'Os norte-americanos são uma nação superior', 're'=>'O PIB argentino é maior que o dos brasileiros', 'rcorreta'=>'D', 'verificada'=>1],
            ['materia_id'=>	3	, 'pergunta' => 'O que é identidade social?', 'ra'=>'É a vinculação do indivíduo em programas sociais', 'rb'=>'É o sentimento de pertencer a uma coletividade específica ', 'rc'=>'É o fato de fazer parte de uma tribo ', 'rd'=>'É o sentimento de identificação com a própria família ', 're'=>'É a vinculação do indivíduo a um grupo religioso', 'rcorreta'=>'B', 'verificada'=>1],
            ['materia_id'=>	3	, 'pergunta' => 'A Sociologia é a ciência que estuda:', 'ra'=>'A relação do homem com a natureza', 'rb'=>'O homem e sua religião ', 'rc'=>'Os grupos sociais e suas organizações ', 'rd'=>'A metafísica', 're'=>'As relações de produção ', 'rcorreta'=>'C', 'verificada'=>1],
            ['materia_id'=>	3	, 'pergunta' => 'Quem usou o termo "Sociologia" pela primeira vez?', 'ra'=>'Augusto Comte', 'rb'=>'Karl Marx', 'rc'=>'Jean Jacques Rousseau', 'rd'=>'Platão ', 're'=>'Aristóteles ', 'rcorreta'=>'A', 'verificada'=>1],
            ['materia_id'=>	3	, 'pergunta' => 'São direitos sociais:', 'ra'=>'Direito à liberdade e direito de ir e vir ', 'rb'=>'Direito de votar e ser votado ', 'rc'=>'Direito à moradia digna e à saúde ', 'rd'=>'Direito de participar de partidos e sindicatos', 're'=>'Direito ao seguro-desemprego  e à justiça', 'rcorreta'=>'C', 'verificada'=>1],
            
             

           
         ]);
    }
}
