<?php

use Illuminate\Database\Seeder;



//php artisan passport:client --personal 
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(MateriaSeeder::class);
        $this->call(PerguntaSeeder::class);
    }
}
