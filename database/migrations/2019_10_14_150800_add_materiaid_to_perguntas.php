<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMateriaidToPerguntas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('perguntas', function (Blueprint $table) {
            $table->bigInteger('materia_id')->nullable()->unsigned();     
        });

        Schema::table('perguntas', function (Blueprint $table) {
            $table->foreign('materia_id')->references('id')->on('materias');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perguntas', function (Blueprint $table) {
            //
        });
    }
}
